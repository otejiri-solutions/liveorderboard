package com.silverbars.liveorderboard.services;

import com.silverbars.liveorderboard.dao.InMemoryOrderDAO;
import com.silverbars.liveorderboard.dao.OrderDAO;
import com.silverbars.liveorderboard.dto.OrderDTO;
import com.silverbars.liveorderboard.dto.SummaryDTO;
import com.silverbars.liveorderboard.enums.OrderType;

import java.util.Arrays;

public class OrderService {

    private OrderDAO orderDAO;

    public OrderService() {
        orderDAO = new InMemoryOrderDAO();
    }

    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    /**
     *
     * @param orderDTO
     *
     * @exception IllegalArgumentException if the the following conditions are not met:
     *         <ol>
     *             <li>The user id is null or empty</li>
     *             <li>The quantity supplied is 0</li>
     *             <li>The price per kg is 0</li>
     *             <li>The type of order type is null, empty or is not one of the following "SELL" or "BUY"</li>
     *         </ol>
     */
    public void registerOrder(OrderDTO orderDTO){
        validateOrder(orderDTO);
        orderDAO.registerOrder(orderDTO);
    }

    /**
     *
     * @param orderDTO
     *
     * @exception IllegalArgumentException if the the following conditions are not met:
     *         <ol>
     *             <li>The user id is null or empty</li>
     *             <li>The quantity supplied is 0</li>
     *             <li>The price per kg is 0</li>
     *             <li>The type of order type is null, empty or is not one of the following "SELL" or "BUY"</li>
     *         </ol>
     */
    public void cancelOrder(OrderDTO orderDTO){
        validateOrder(orderDTO);
        orderDAO.cancelOrder(orderDTO);
    }

    public SummaryDTO getSummary(){
        return orderDAO.getSummary();
    }

    private void validateOrder(OrderDTO orderDTO){

        if(orderDTO.getUserId() == null && orderDTO.getUserId().trim().isEmpty())
            throw new IllegalArgumentException("An Order must contain a [user id]");

        if(orderDTO.getOrderType() == null)
            throw new IllegalArgumentException("The type of Order must be specified valid types are " + Arrays.toString(OrderType.values()) );

        if(orderDTO.getQuantity() <= 0)
            throw new IllegalArgumentException("The Order quantity provided must be more than zero ");

        if(orderDTO.getPricePerKg() <= 0)
            throw new IllegalArgumentException("The Order price per kg provided must be more than zero ");
    }

}
