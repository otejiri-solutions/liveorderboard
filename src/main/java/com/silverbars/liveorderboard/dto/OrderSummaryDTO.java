package com.silverbars.liveorderboard.dto;

import java.util.Objects;

public class OrderSummaryDTO {
    private final double pricePerKg;
    private final double quantitySum;

    public OrderSummaryDTO(double pricePerKg, double quantitySum) {
        this.quantitySum = quantitySum;
        this.pricePerKg = pricePerKg;
    }

    public double getQuantitySum() {
        return quantitySum;
    }

    public double getPricePerKg() {
        return pricePerKg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderSummaryDTO that = (OrderSummaryDTO) o;
        return Double.compare(that.pricePerKg, pricePerKg) == 0 &&
                Double.compare(that.quantitySum, quantitySum) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pricePerKg, quantitySum);
    }

    @Override
    public String toString() {
        return "OrderSummaryDTO{" +
                "pricePerKg=" + pricePerKg +
                ", quantitySum=" + quantitySum +
                '}';
    }
}
