package com.silverbars.liveorderboard.dto;

import java.util.Collection;

public class SummaryDTO {

    private final Collection<OrderSummaryDTO> sellOrdersSummary;

    private final Collection<OrderSummaryDTO> buyOrdersSummary;

    public SummaryDTO(Collection<OrderSummaryDTO> sellOrdersSummary, Collection<OrderSummaryDTO> buyOrdersSummary) {
        this.sellOrdersSummary = sellOrdersSummary;
        this.buyOrdersSummary = buyOrdersSummary;
    }

    public Collection<OrderSummaryDTO> getSellOrdersSummary() {
        return sellOrdersSummary;
    }

    public Collection<OrderSummaryDTO> getBuyOrdersSummary() {
        return buyOrdersSummary;
    }
}
