package com.silverbars.liveorderboard.dto;

import com.silverbars.liveorderboard.enums.OrderType;

import java.util.Objects;

public class OrderDTO {
    private final String userId;
    private final double quantity;
    private final double pricePerKg;
    private final OrderType orderType;

    public OrderDTO(String userId, double quantity, double pricePerKg, OrderType orderType) {
        this.userId = userId;
        this.quantity = quantity;
        this.pricePerKg = pricePerKg;
        this.orderType = orderType;
    }

    public String getUserId() {
        return userId;
    }

    public double getQuantity() {
        return quantity;
    }

    public double getPricePerKg() {
        return pricePerKg;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderDTO orderDTO = (OrderDTO) o;
        return Double.compare(orderDTO.quantity, quantity) == 0 &&
                Double.compare(orderDTO.pricePerKg, pricePerKg) == 0 &&
                Objects.equals(userId, orderDTO.userId) &&
                orderType == orderDTO.orderType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, quantity, pricePerKg, orderType);
    }
}
