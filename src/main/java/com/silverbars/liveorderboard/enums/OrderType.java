package com.silverbars.liveorderboard.enums;

public enum OrderType {
    BUY,
    SELL
}
