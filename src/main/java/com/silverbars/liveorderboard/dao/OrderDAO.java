package com.silverbars.liveorderboard.dao;

import com.silverbars.liveorderboard.dto.OrderDTO;
import com.silverbars.liveorderboard.dto.SummaryDTO;

public interface OrderDAO {

    void registerOrder(OrderDTO orderDTO);

    void cancelOrder(OrderDTO orderDTO);

    SummaryDTO getSummary();

}
