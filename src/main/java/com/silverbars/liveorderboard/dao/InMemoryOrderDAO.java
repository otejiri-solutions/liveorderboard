package com.silverbars.liveorderboard.dao;

import com.silverbars.liveorderboard.dto.OrderDTO;
import com.silverbars.liveorderboard.dto.OrderSummaryDTO;
import com.silverbars.liveorderboard.dto.SummaryDTO;
import com.silverbars.liveorderboard.enums.OrderType;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;

public class InMemoryOrderDAO implements OrderDAO {

    private static class PriceComparator implements Comparator<Double> {

        private OrderType orderType;

        public PriceComparator(OrderType orderType) {
            this.orderType = orderType;
        }

        @Override
        public int compare(Double o1, Double o2) {
            if(o1.doubleValue() == o2.doubleValue()){
                return 0;
            }else{
                if(orderType == OrderType.BUY){
                    return  (o1.doubleValue() > o2.doubleValue()) ? -1 : 1;
                }else{
                    return  (o1.doubleValue() > o2.doubleValue()) ? 1 : -1;
                }
            }
        }
    }

    private final ConcurrentMap<OrderDTO,Boolean> sellOrders;
    private final ConcurrentMap<OrderDTO,Boolean> buyOrders;
    private final Map<Double,OrderSummaryDTO> priceToSellOrdersSummaryMap;
    private final Map<Double,OrderSummaryDTO> priceToBuyOrdersSummaryMap;

    public InMemoryOrderDAO() {
        sellOrders = new ConcurrentHashMap<>();
        buyOrders = new ConcurrentHashMap<>();
        priceToBuyOrdersSummaryMap  = new ConcurrentSkipListMap<>(new PriceComparator(OrderType.BUY));
        priceToSellOrdersSummaryMap  = new ConcurrentSkipListMap<>(new PriceComparator(OrderType.SELL));
    }

    @Override
    public void registerOrder(OrderDTO orderDTO){
        if(orderDTO.getOrderType() == OrderType.SELL)
            registerOrder(orderDTO,sellOrders,priceToSellOrdersSummaryMap);
        else
            registerOrder(orderDTO,buyOrders,priceToBuyOrdersSummaryMap);
    }

    private void registerOrder(OrderDTO orderDTO, ConcurrentMap<OrderDTO,Boolean> orderMap, Map<Double,OrderSummaryDTO> priceToOrdersSummaryMap){
        orderMap.computeIfAbsent(orderDTO, order1 -> {
            priceToOrdersSummaryMap.computeIfPresent(orderDTO.getPricePerKg(),(number, orderSummary) ->
                new OrderSummaryDTO(orderDTO.getPricePerKg(), orderDTO.getQuantity()+orderSummary.getQuantitySum())
            );

            priceToOrdersSummaryMap.computeIfAbsent(
                    orderDTO.getPricePerKg(),
                    number -> new OrderSummaryDTO(orderDTO.getPricePerKg(), orderDTO.getQuantity())
            );

            return true;
        });
    }

    @Override
    public void cancelOrder(OrderDTO orderDTO){
        if(orderDTO.getOrderType() == OrderType.SELL)
            cancelOrder(orderDTO,sellOrders,priceToSellOrdersSummaryMap);
        else
            cancelOrder(orderDTO,buyOrders,priceToBuyOrdersSummaryMap);
    }

    private void cancelOrder(OrderDTO orderDTO, ConcurrentMap<OrderDTO,Boolean> orderMap, Map<Double,OrderSummaryDTO> priceToOrdersSummaryMap){
        orderMap.computeIfPresent(orderDTO,(order1, aBoolean) -> {

            //Reduce the quantity sum, if the new sum is not greater than zero then the mapping is removed
            priceToOrdersSummaryMap.compute(orderDTO.getPricePerKg(),(number, orderSummary) -> {
                double quantitySum = orderSummary.getQuantitySum() - orderDTO.getQuantity();
                if(quantitySum > 0)
                    return new OrderSummaryDTO(orderDTO.getPricePerKg(), quantitySum);
                else
                    return null;
            });

            return orderMap.remove(orderDTO);
        });
    }

    @Override
    public SummaryDTO getSummary() {
        final Collection<OrderSummaryDTO> sellOrdersSummary = Collections.unmodifiableCollection(priceToSellOrdersSummaryMap.values());
        final Collection<OrderSummaryDTO> buyOrdersSummary = Collections.unmodifiableCollection(priceToBuyOrdersSummaryMap.values());

        return new SummaryDTO(sellOrdersSummary,buyOrdersSummary);
    }

}

