package com.silverbars.liveorderboard;

import com.silverbars.liveorderboard.dto.OrderDTO;
import com.silverbars.liveorderboard.dto.SummaryDTO;

public class LiveOrderBoardTestInfo {


    private OrderDTO orderDTO;

    private SummaryDTO expectedSummaryDTO;

    private OrderDTO[] orderDTOsToAddBeforeTest;

    private Class<? extends Throwable> expectedExceptionCl;

    public LiveOrderBoardTestInfo(OrderDTO orderDTO, SummaryDTO expectedSummaryDTO) {
        this(orderDTO);
        this.expectedSummaryDTO = expectedSummaryDTO;
    }

    public LiveOrderBoardTestInfo(OrderDTO orderDTO, Class<? extends Throwable> expectedExceptionCl) {
        this(orderDTO);
        this.expectedExceptionCl = expectedExceptionCl;
    }

    public LiveOrderBoardTestInfo(OrderDTO orderDTO, SummaryDTO expectedSummaryDTO, OrderDTO[] orderDTOsToAddBeforeTest) {
        this(orderDTO,expectedSummaryDTO);
        this.orderDTOsToAddBeforeTest = orderDTOsToAddBeforeTest;
    }


    public LiveOrderBoardTestInfo(SummaryDTO expectedSummaryDTO, OrderDTO[] orderDTOsToAddBeforeTest){
        this.expectedSummaryDTO = expectedSummaryDTO;
        this.orderDTOsToAddBeforeTest = orderDTOsToAddBeforeTest;
    }


    private LiveOrderBoardTestInfo(OrderDTO orderDTO){
        this.orderDTO = orderDTO;
    }


    public OrderDTO getOrderDTO() {
        return orderDTO;
    }

    public SummaryDTO getExpectedSummaryDTO() {
        return expectedSummaryDTO;
    }

    public OrderDTO[] getOrderDTOsToAddBeforeTest() {
        return orderDTOsToAddBeforeTest;
    }

    public Class<? extends Throwable> getExpectedExceptionCl() {
        return expectedExceptionCl;
    }
}
