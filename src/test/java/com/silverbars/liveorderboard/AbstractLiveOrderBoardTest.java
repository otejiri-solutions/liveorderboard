package com.silverbars.liveorderboard;

import com.silverbars.liveorderboard.dto.OrderDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.function.Consumer;

@ExtendWith(SpringExtension.class)
public abstract class AbstractLiveOrderBoardTest {

    protected LiveOrderBoardTestInfo liveOrderBoardTestInfo;

    @Autowired
    protected ApplicationContext applicationContext;

    private Consumer<OrderDTO> preTestRegisterOrderFunction;

    protected void setPreTestRegisterOrderFunction(Consumer<OrderDTO> function) {
        this.preTestRegisterOrderFunction = function;
    }

    @BeforeEach
    public void before(TestInfo testInfo){

        liveOrderBoardTestInfo = applicationContext.getBean(testInfo.getDisplayName(),LiveOrderBoardTestInfo.class);

        //Check if there are words to be added before the test is done
        OrderDTO[] orderDTOsToAddBeforeTest = liveOrderBoardTestInfo.getOrderDTOsToAddBeforeTest();
        if(orderDTOsToAddBeforeTest != null) {
            for (OrderDTO orderDTO : orderDTOsToAddBeforeTest)
                preTestRegisterOrderFunction.accept(orderDTO);
        }
    }

}
