package com.silverbars.liveorderboard.dao;

import com.silverbars.liveorderboard.AbstractLiveOrderBoardTest;
import com.silverbars.liveorderboard.dto.SummaryDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.HashSet;

@ContextConfiguration("/orderDAO-context.xml")
public class OrderDAOTest extends AbstractLiveOrderBoardTest {

    private OrderDAO orderDAO;

    OrderDAOTest() {
        orderDAO = new InMemoryOrderDAO();
        setPreTestRegisterOrderFunction(orderDAO::registerOrder);
    }

    private void doRegisterOrder() {
        orderDAO.registerOrder(liveOrderBoardTestInfo.getOrderDTO());
        assertSummaryIsEqual();
    }

    private void doCancelOrder(){
        orderDAO.cancelOrder(liveOrderBoardTestInfo.getOrderDTO());
        assertSummaryIsEqual();
    }

    @Test
    @DisplayName("getSummaryFromEmptyLiveBoard")
    void getSummaryFromEmptyLiveBoard(){
        assertSummaryIsEqual();
    }


    @Test
    @DisplayName("getSummaryFromLiveBoardWithExistingOrders")
    void getSummaryFromLiveBoardWithExistingOrders(){
        assertSummaryIsEqual();
    }


    @Test
    @DisplayName("summaryWithDifferentElementSequenceShouldFail")
    void summaryWithDifferentElementSequenceShouldFail(){
        SummaryDTO expectedSummaryDTO = liveOrderBoardTestInfo.getExpectedSummaryDTO();
        SummaryDTO summaryDTO = orderDAO.getSummary();

        //Assert that the collections are ordered differently
        Assertions.assertNotEquals(
                new ArrayList<>(expectedSummaryDTO.getSellOrdersSummary()),
                new ArrayList<>(summaryDTO.getSellOrdersSummary())
        );
        //Assert that the collections are ordered differently but contain the same elements
        Assertions.assertEquals(
                new HashSet<>(expectedSummaryDTO.getSellOrdersSummary()),
                new HashSet<>(summaryDTO.getSellOrdersSummary())
        );


    }


    @Test
    @DisplayName("registerOrderToEmptyLiveBoard")
    void registerOrderToEmptyLiveBoard() {
        doRegisterOrder();
    }

    @Test
    @DisplayName("registerOrderToLiveBoardWithExistingOrders")
    void registerOrderToLiveBoardWithExistingOrders(){
        doRegisterOrder();
    }


    @Test
    @DisplayName("cancelOrderFromEmptyLiveBoard")
    void cancelOrderFromEmptyLiveBoard(){
        doCancelOrder();
    }

    @Test
    @DisplayName("cancelOrderFromLiveBoardWithExistingOrders")
    void cancelOrderFromLiveBoardWithExistingOrders(){
        doCancelOrder();
    }


    @Test
    @DisplayName("cancelOrderNotInLiveBoardWithExistingOrders")
    void cancelOrderNotInLiveBoardWithExistingOrders(){
        doCancelOrder();
    }


    private void assertSummaryIsEqual(){
        SummaryDTO expectedSummaryDTO = liveOrderBoardTestInfo.getExpectedSummaryDTO();
        SummaryDTO summaryDTO = orderDAO.getSummary();

        Assertions.assertEquals(
                new ArrayList<>(expectedSummaryDTO.getSellOrdersSummary()),
                new ArrayList<>(summaryDTO.getSellOrdersSummary())
        );
        Assertions.assertEquals(
                new ArrayList<>(expectedSummaryDTO.getBuyOrdersSummary()),
                new ArrayList<>(summaryDTO.getBuyOrdersSummary())
        );
    }

}
