package com.silverbars.liveorderboard.services;

import com.silverbars.liveorderboard.AbstractLiveOrderBoardTest;
import com.silverbars.liveorderboard.dao.OrderDAO;
import com.silverbars.liveorderboard.dto.OrderDTO;
import com.silverbars.liveorderboard.dto.SummaryDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ContextConfiguration("/orderService-context.xml")
public class OrderServiceTest extends AbstractLiveOrderBoardTest {

    private OrderService orderService;

    OrderServiceTest() {
        orderService = new OrderService();
        setPreTestRegisterOrderFunction(orderService::registerOrder);
    }

    @Test
    @DisplayName("registerOrder")
    void registerOrder() {
        Assertions.assertAll(() -> orderService.registerOrder(liveOrderBoardTestInfo.getOrderDTO()));
    }

    @Test
    @DisplayName("cancelOrder")
    void cancelOrder() {
        Assertions.assertAll(() -> orderService.cancelOrder(liveOrderBoardTestInfo.getOrderDTO()));
    }

    @Test
    @DisplayName("registerOrderThrowsException")
    void registerOrderThrowsException(){
        Class<? extends Throwable> expectedExceptionCl = liveOrderBoardTestInfo.getExpectedExceptionCl();
        OrderDTO orderDTO = liveOrderBoardTestInfo.getOrderDTO();

        Assertions.assertThrows(expectedExceptionCl, () -> orderService.registerOrder(orderDTO));
    }

    @Test
    @DisplayName("cancelOrderThrowsException")
    void cancelOrderThrowsException(){
        Class<? extends Throwable> expectedExceptionCl = liveOrderBoardTestInfo.getExpectedExceptionCl();
        OrderDTO orderDTO = liveOrderBoardTestInfo.getOrderDTO();

        Assertions.assertThrows(expectedExceptionCl, () -> orderService.cancelOrder(orderDTO));
    }

    @Test
    @DisplayName("getSummary")
    void doGetSummary() {

        SummaryDTO expectedSummaryDTO = liveOrderBoardTestInfo.getExpectedSummaryDTO();
        OrderDAO orderDAO = mock(OrderDAO.class);
        orderService.setOrderDAO(orderDAO);
        when(orderDAO.getSummary()).thenReturn(expectedSummaryDTO);

        SummaryDTO summaryDTO = orderService.getSummary();

        Assertions.assertEquals(expectedSummaryDTO,summaryDTO);
    }

}
